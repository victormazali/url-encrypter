# frozen_string_literal: true

class Entry < ApplicationRecord
  before_save :fill_up_token

  def fill_up_token
    self.token ||= SecureRandom.uuid
  end
end
