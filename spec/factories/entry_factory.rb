FactoryBot.define do
  factory :entry, class: Entry do
    url { 'https://www.google.com' }
    token { SecureRandom.uuid }
  end
end
