RSpec.describe Entry, type: :model do

  context '#fill_up_token' do
    let(:instance) { build(:entry, token: token) }
    let(:result) { instance.fill_up_token }

    context 'when token is already set' do
      let(:token) { 'abc' }

      it 'keeps the same token' do
        result
        expect(instance.token).to eq('abc')
      end
    end

    context 'when token is not set yet' do
      let(:token) { nil }

      it 'generates a token' do
        result

        expect(instance.token).to be_present
        expect(instance.token).to match(/[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}/)
      end
    end
  end
end
